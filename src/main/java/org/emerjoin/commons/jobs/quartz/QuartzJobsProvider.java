package org.emerjoin.commons.jobs.quartz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emerjoin.commons.jobs.*;
import org.emerjoin.commons.jobs.Job;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.Bean;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

@ApplicationScoped
public class QuartzJobsProvider implements JobsProvider {

    private QuartzScheduleCtl quartzScheduleCtl;
    private Scheduler scheduler;
    private Map<String,JobSpec> jobSpecMap = new HashMap<>();

    protected static final String SCHEDULED_JOBS_TAG = "scheduled";
    private static final Logger LOGGER = LogManager.getLogger(QuartzJobsProvider.class);

    private Properties configs = new Properties();

    @Inject
    public QuartzJobsProvider(@QuartzConfig Instance<Properties> bean){
        if(bean!=null){
            if(!bean.isUnsatisfied()){
                LOGGER.info("@QuartzConfig producer found. Obtaining configs");
                this.configs = bean.get();
                return;
            }
        }
        LOGGER.warn("There is no @QuartzConfig producer found. Will use default configs");
    }


    private JobSpec getJobSpec(Class<? extends Job> type){
        JobSpec spec = jobSpecMap.get(type.getCanonicalName());
        if(spec==null){
            spec = new JobSpec(type);
            jobSpecMap.put(type.getCanonicalName(),spec);
        }
        return spec;
    }

    @Override
    public void initialize(JobsProviderContext context) {
        if(context==null)
            throw new IllegalArgumentException("context must not be null");
        LOGGER.info("Initializing Quartz Jobs...");
        this.quartzScheduleCtl = new QuartzScheduleCtl(this.scheduler);
        try {
            SchedulerFactory schedulerFactory = null;
            if(configs.isEmpty()){
                schedulerFactory = new StdSchedulerFactory();
            }else schedulerFactory = new StdSchedulerFactory(configs);
            this.scheduler = schedulerFactory.getScheduler();
            this.scheduler.setJobFactory(new QuartzJobFactory(context));
            this.scheduler.start();
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error Initializing Quartz scheduler",
                    ex);
        }
    }

    @Override
    public void teardown() {
        LOGGER.info("Tearing down Quartz Jobs...");
        if(scheduler!=null) {
            try {
                scheduler.shutdown();
            } catch (SchedulerException ex) {
                throw new QuartzJobsInternalException("error shutting down scheduler",
                        ex);
            }
        }
    }


    @Override
    public SchedulerCtl scheduler() {
        if(this.quartzScheduleCtl==null)
            throw new IllegalStateException("scheduler not initialized");
        return this.quartzScheduleCtl;
    }


    @Override
    public boolean supports(Class<? extends Job> type) {
        //Supports all Job types
        return true;
    }

    @Override
    public JobSchedule getJobSchedule(Class<? extends Job> type) throws JobsProviderException {
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        TriggerKey triggerKey = this.makeTriggerKey(type, SCHEDULED_JOBS_TAG);
        return new JobScheduleImpl(triggerKey,
                scheduler);
    }


    private JobDataMap toDataMap(JobParams params){
        JobDataMap dataMap = new JobDataMap();
        params.values().forEach(dataMap::put);
        return dataMap;
    }

    private JobKey makeJobKey(Class<? extends Job> type){
        return JobKey.jobKey(type.getSimpleName(),type.getCanonicalName());
    }

    private TriggerKey makeTriggerKey(Class<? extends Job> type, String tag){
        return TriggerKey.triggerKey(tag,type.getCanonicalName());
    }



    private void addJobIfNotYet(Class<? extends Job> type, JobKey key){
        try {
            JobDetail detail = scheduler.getJobDetail(key);
            if (detail == null) {
                JobDetail jobDetail = makeJobDetail(type, key);
                this.scheduler.addJob(jobDetail, true);
            }
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error adding Job to scheduler: "+type.getCanonicalName(),
                    ex);
        }
    }

    private JobDetail makeJobDetail(Class<? extends Job> type, JobKey jobKey){
        JobSpec spec = getJobSpec(type);
        return JobBuilder.newJob(spec.quartzJobType())
                .withIdentity(jobKey)
                .storeDurably()
                .build();
    }

    @Override
    public JobExecutionId execute(Class<? extends Job> type) {
        return this.execute(type,JobParams.empty());
    }

    @Override
    public JobExecutionId execute(Class<? extends Job> type, JobParams jobParams) {
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(jobParams==null)
            throw new IllegalArgumentException("jobParams must not be null");
        JobKey jobKey = makeJobKey(type);
        JobExecutionId jobExecutionId = new JobExecutionId(UUID.randomUUID().toString());
        jobParams.put(Constants.EXECUTION_ID,jobExecutionId.value());
        this.addJobIfNotYet(type,jobKey);
        try {
            this.scheduler.triggerJob(jobKey, toDataMap(jobParams));
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error triggering job: "+type.getCanonicalName(),
                    ex);
        }
        return jobExecutionId;
    }

    @Override
    public void schedule(Class<? extends Job> type, String expression) throws JobsProviderException {
        this.schedule(type,expression,JobParams.empty());
    }

    @Override
    public void schedule(Class<? extends Job> type, String expression, JobParams jobParams) throws JobsProviderException {
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(expression==null||expression.isEmpty())
            throw new IllegalArgumentException("expression must not be null nor empty");
        if(jobParams==null)
            throw new IllegalArgumentException("jobParams must not be null");
        this.schedule(type, type.getSimpleName()+"-"+SCHEDULED_JOBS_TAG,expression,jobParams);
    }


    private void schedule(Class<? extends Job> type, String tag, String expression) throws JobsProviderException {
        this.schedule(type,tag,expression,JobParams.empty());
    }

    private void schedule(Class<? extends Job> type, String tag, String expression, JobParams jobParams) throws JobsProviderException {
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(tag==null||tag.isEmpty())
            throw new IllegalArgumentException("tag must not be null nor empty");
        if(expression==null||expression.isEmpty())
            throw new IllegalArgumentException("expression must not be null nor empty");
        if(jobParams==null)
            throw new IllegalArgumentException("jobParams must not be null");
        JobKey jobKey = makeJobKey(type);
        this.addJobIfNotYet(type,jobKey);
        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .forJob(jobKey)
                .withSchedule(CronScheduleBuilder.cronSchedule(expression))
                .usingJobData(toDataMap(jobParams))
                .withIdentity(makeTriggerKey(type,tag))
                .build();
        try {
            this.scheduler.scheduleJob(cronTrigger);
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException(String.format("error scheduling job %s with tag=%s",type.getCanonicalName(),tag),
                    ex);
        }

    }


}
