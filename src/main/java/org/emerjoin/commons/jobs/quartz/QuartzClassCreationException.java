package org.emerjoin.commons.jobs.quartz;

public class QuartzClassCreationException extends QuartzJobsException {

    public QuartzClassCreationException(String message) {
        super(message);
    }

    public QuartzClassCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}
