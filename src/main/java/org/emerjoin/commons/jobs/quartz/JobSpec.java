package org.emerjoin.commons.jobs.quartz;

import javassist.*;

import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emerjoin.commons.jobs.Job;
import org.emerjoin.commons.jobs.JobInfo;
import org.quartz.*;

import java.util.HashMap;
import java.util.Map;

public class JobSpec {

    private static final Map<String,Class<? extends org.quartz.Job>> classMap = new HashMap<>();
    private static final ClassPool classPool = ClassPool.getDefault();
    private static final Logger LOGGER = LogManager.getLogger(JobSpec.class);

    private Class<? extends Job> jobType;
    private JobInfo jobInfo;

    static {
        classPool.appendClassPath(new ClassClassPath(AbstractQuartzJob.class));
        classPool.importPackage("org.emerjoin.commons.jobs.quartz");
    }

    public JobSpec(Class<? extends Job> type){
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        this.jobType = type;
        this.jobInfo = jobType.getAnnotation(JobInfo.class);
        if(jobInfo==null)
            throw new IllegalArgumentException(String.format("@%s missing on Job Type: %s",JobInfo.class.getSimpleName(),
                    type.getCanonicalName()));
    }

    public Class<? extends org.quartz.Job> quartzJobType(){
        Class<? extends org.quartz.Job> quartzJobType = classMap.get(jobType.getCanonicalName());
        try {
            if (quartzJobType == null) {
                LOGGER.info("Creating Quartz Job type for "+jobType.getCanonicalName());
                CtClass ctBaseClass = classPool.getCtClass(AbstractQuartzJob.class.getCanonicalName());
                CtClass quartzCtClass = classPool.makeClass(jobType.getCanonicalName() + "QuartzJob");
                ClassFile quartzCtClassFile = quartzCtClass.getClassFile();
                quartzCtClass.setSuperclass(ctBaseClass);
                ConstPool quartzConstPool = quartzCtClassFile.getConstPool();
                AnnotationsAttribute attr = new AnnotationsAttribute(quartzConstPool, AnnotationsAttribute.visibleTag);
                if(jobInfo.singleton()) {
                    attr.addAnnotation(new Annotation(DisallowConcurrentExecution.class.getTypeName(),
                            quartzConstPool));
                }
                /*
                if(Stateful.class.isAssignableFrom(jobType)){
                    attr.addAnnotation(new Annotation(PersistJobDataAfterExecution.class.getTypeName(),
                            quartzConstPool));
                }*/
                quartzCtClassFile.addAttribute(attr);
                quartzCtClassFile.setVersionToJava5();

                CtConstructor defaultConstructor = CtNewConstructor.defaultConstructor(quartzCtClass);
                defaultConstructor.setBody(String.format("super(%s.class);",jobType.getCanonicalName()));
                quartzCtClass.addConstructor(defaultConstructor);
                quartzJobType = (Class<? extends org.quartz.Job>) quartzCtClass.toClass();
                LOGGER.info("Created Quartz Job type: "+quartzJobType.getCanonicalName());
                classMap.put(jobType.getCanonicalName(), quartzJobType);
            }else LOGGER.info("Returning existing Quartz Job Type: "+quartzJobType.getCanonicalName());
        }catch (CannotCompileException | NotFoundException ex){
            throw new QuartzClassCreationException("error creating quartz class",
                    ex);
        }
        return quartzJobType;
    }


}
